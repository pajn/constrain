library constrain.validator.function.proxy.test;

import 'package:constrain/constrain.dart';
import 'package:constrain/constrained_function_proxy.dart';
import 'package:unittest/unittest.dart';
import 'dart:async';
import 'dart:mirrors';

main() {
  final validator = new Validator();
  final proxy = new ConstrainedFunctionProxy(validator);
  final test1Mirror = reflect(test1);
  final test2Mirror = reflect(test2);

  invokeTest1(String foo, int age,
      { bool validateParameters: true, bool validateReturn: true }) {
    return (age != null) ?
      proxy.invoke(test1Mirror, [foo],
          namedArguments: { #person: new Person(age) },
          validateParameters: validateParameters,
          validateReturn: validateReturn)
        : proxy.invoke(test1Mirror, [foo],
            validateParameters: validateParameters,
            validateReturn: validateReturn);
  }

  invokeTest2(String foo, int age,
              { bool validateParameters: true, bool validateReturn: true }) {
    return (age != null) ?
      proxy.invoke(test2Mirror, [foo],
          namedArguments: { #person: new Person(age) },
          validateParameters: validateParameters,
          validateReturn: validateReturn)
        : proxy.invoke(test2Mirror, [foo],
            validateParameters: validateParameters,
            validateReturn: validateReturn);
  }


  group('invoke', (){
    group('when no constraints are violated', () {
      group('returns a normal result', () {
        invoke() => invokeTest1("howdy", 10);

        test('that is not null', () {
          expect(invoke(), isNotNull);
        });

        test('that is as expected', () {
          expect(invoke(), ageThat(equals('howdy')));
        });
      });

      group('returns a future result', () {
        invoke() => invokeTest2("howdy", 10);

        test('that is not null', () {
          expect(invoke(), isNotNull);
        });

        test('that is future', () {
          expect(invoke(), completes);
        });

        test('that is not null future value', () {
          expect(invoke(), completion(isNotNull));
        });

        test('that is as expected', () {
          expect(invoke(), completion(ageThat(equals('howdy'))));
        });

      });

    });

    group('when parameter constraints are violated', () {
      group('and return is normal throws an exception', () {

        test('- positional parameter case', () {
          expect(() => invokeTest1(null, 10),
              throwsA(new isInstanceOf<ParameterConstraintViolationException>()));
        });

        test('- named parameter case', () {
          expect(() => invokeTest1("howdy", null),
              throwsA(new isInstanceOf<ParameterConstraintViolationException>()));
        });
      });

      group('and return is a future returns an error', () {

        test('- positional parameter case', () {
          expect(invokeTest2(null, 10),
              throwsA(new isInstanceOf<ParameterConstraintViolationException>()));
        });

        test('- named parameter case', () {
          expect(invokeTest2("howdy", null),
              throwsA(new isInstanceOf<ParameterConstraintViolationException>()));
        });
      });

      group('and validateParameters is false', () {
        group('returns a normal result', () {
          invoke() => invokeTest1("howdy", null, validateParameters: false);

          test('that is not null', () {
            expect(invoke(), isNotNull);
          });

          test('that is as expected', () {
            expect(invoke(), ageThat(equals('howdy')));
          });
        });

        group('returns a future result', () {
          invoke() => invokeTest2("howdy", null, validateParameters: false);

          test('that is not null', () {
            expect(invoke(), isNotNull);
          });

          test('that is future', () {
            expect(invoke(), completes);
          });

          test('that is not null future value', () {
            expect(invoke(), completion(isNotNull));
          });

          test('that is as expected', () {
            expect(invoke(), completion(ageThat(equals('howdy'))));
          });
        });

      });

    });

    group('when return constraints are violated', () {
      group('on a normal result', () {

        test('', () {
          expect(() => invokeTest1("null", 10),
              throwsA(new isInstanceOf<ReturnConstraintViolationException>()));
        });

        group('and validateReturn is false', () {
          group('returns a normal result', () {
            invoke() => invokeTest1("null", 10, validateReturn: false);

            test('that is null', () {
              expect(invoke(), isNull);
            });

          });

        });
      });

      group('on a future result', () {

        test('', () {
          expect(invokeTest2("null", 10),
              throwsA(new isInstanceOf<ReturnConstraintViolationException>()));
        });

        group('and validateReturn is false', () {
          group('returns a future result', () {
            invoke() => invokeTest2("null", 10, validateReturn: false);

            test('that is null', () {
              expect(invoke(), completion(isNull));
            });

          });

        });

      });
    });
  });
}

Matcher ageThat(Matcher matcher) =>
  fieldMatcher("Registration", "id", matcher, (Registration r) => r.id);

class Person {
  @NotNull()
  int age;

  Person(this.age);
}

class Registration {
  @NotNull()
  String id;

  Registration(this.id);
}

@NotNull() Registration test1(@NotNull() String foo,
                              { @NotNull() Person person } ) =>
    foo == "null" ? null : new Registration(foo);

@NotNull() Future<Registration> test2(@NotNull() String foo,
                                      { @NotNull() Person person } ) =>
    foo == "null" ? new Future.value(null) :
      new Future.value(new Registration(foo));