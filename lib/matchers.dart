// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.matchers;

import 'package:matcher/matcher.dart';



typedef Getter(object);

Matcher isNotEmpty() => isNot(isEmpty);

Matcher fieldMatcher(String className, String fieldName, matcher, 
                     Getter getter) =>
    new FieldMatcher(className, fieldName, matcher, getter);

class FieldMatcher extends CustomMatcher {
  final Getter getter;
  
  FieldMatcher(String className, String fieldName, matcher, this.getter) 
      : super("$className with $fieldName that", fieldName, matcher);
  
  featureValueOf(actual) => getter(actual);
}
