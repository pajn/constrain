// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.metadata;

import 'constraint.dart';
import 'src/preconditions.dart';
import 'src/runtime_constraint_resolver.dart';
import 'package:collection/collection.dart';
import 'package:option/option.dart';
import 'package:quiver/collection.dart';
import 'src/util.dart';

abstract class TypeDescriptorResolver {
  factory TypeDescriptorResolver() = RuntimeTypeDescriptorResolver;

  Option<TypeDescriptor> resolveFor(Type clazz);

  // TODO: this is inconsistent as a Function is not at the same meta model
  // level as type
  Option<FunctionDescriptor> resolveForFunction(Function function);
}

// Not sure if we need this or not. Java's one has a fair load of stuff but
// we may be able to stick at least some of them on Constraint itself as Dart's
// annotations are much less sucky
class ConstraintDescriptor<T extends Constraint> {
  final T constraint;

  ConstraintDescriptor(this.constraint) {
    ensure(constraint, isNotNull);
  }

  int get hashCode => constraint.hashCode;

  bool operator==(other) => other is ConstraintDescriptor
      && constraint == other.constraint;

  String toString() => constraint.toString();
}

abstract class ElementDescriptor {
  /// includes all constraints inherited from parent classes,
  /// interfaces and mixins
  final Set<ConstraintDescriptor> constraintDescriptors;

  ElementDescriptor(this.constraintDescriptors) {
    ensure(constraintDescriptors, isNotNull);
  }
}

// TODO: This class is very property specific. Likely want to make
// MemberDescriptor a base class with a sub class for property and method
// Note MethodDescriptor must relate to FunctionDescriptor too
// Although functions are in a way much more like classes. A functions params
// are very much like a classes properties
// TODO: naming of this class needs to change. Maybe InstanceDescriptor.
// It is used for:
// * properties on a class
// * parameters to functions / methods
// * return of functions / methods => but this has null name!!!
// It holds
// * type descriptor
// * constraints on the instance
// * a name (symbol) - doesn't fit for return!!!
class MemberDescriptor extends ElementDescriptor {
  final Symbol name;
  final Option<TypeDescriptor> typeDescriptor;
  final bool isMultivalued;
  final bool isGeneric;
  final bool isFuture;

  MemberDescriptor(this.name, Set<ConstraintDescriptor> constraintDescriptors,
    this.typeDescriptor, 
    {this.isMultivalued: false, this.isGeneric: false, this.isFuture: false})
      : super(constraintDescriptors) {
    ensure(name, isNotNull);
  }

  MemberDescriptor merge(MemberDescriptor other) {
    // TODO: which TypeDescriptor do we use. Is it possible to override a
    // property with a subtype?
    // As long as we always merge so that the most refined type is preserved
    final newConstraints = new Set<ConstraintDescriptor>()
        ..addAll(other.constraintDescriptors)
        ..addAll(constraintDescriptors);
    return new MemberDescriptor(name, newConstraints, typeDescriptor);
  }

  String toString() => 'MemberDescriptor[$name]';

}

/**
 * Provides access to all the constraints on a [type].
 *
 * [Constraint]s on the type itself can be accessed via [constraintDescriptors].
 * These include all constraints inherited from all parent classes, interfaces
 * and mixins.
 *
 * Constraints on members can be accessed via [memberDescriptorsMap] and are
 * similarly inherited.
 */
class TypeDescriptor extends ElementDescriptor {
  final Type type;


  /// includes all member constraints inherited from parent classes,
  /// interfaces and mixins
  final Map<Symbol, MemberDescriptor> memberDescriptorsMap;
  Iterable<MemberDescriptor> get memberDescriptors => memberDescriptorsMap.values;

  final Map<Symbol, FunctionDescriptor> methodDescriptorsMap;

  TypeDescriptor._internal(this.type,
      Set<ConstraintDescriptor> constraintDescriptors,
        this.memberDescriptorsMap, this.methodDescriptorsMap)
      : super(constraintDescriptors) {
    ensure(type, isNotNull);
    ensure(memberDescriptorsMap, isNotNull);
    ensure(methodDescriptorsMap, isNotNull);
  }

  factory TypeDescriptor(Type type,
      Set<ConstraintDescriptor> constraintDescriptors,
      Set<MemberDescriptor> memberDescriptors,
      Set<FunctionDescriptor> methodDescriptors,
      Option<TypeDescriptor> superclassDescriptor,
      Iterable<TypeDescriptor> superinterfaceDescriptors,
      { bool isCollection: false, bool isGeneric: false }) {

    ensure(constraintDescriptors, isNotNull);
    ensure(memberDescriptors, isNotNull);
    ensure(superclassDescriptor, isNotNull);

    final Iterable<TypeDescriptor> inheritedDescriptors =
        []..addAll(optionAsIterable(superclassDescriptor))
        ..addAll(superinterfaceDescriptors);

    final mergedMemberDescriptors = _createMergedMemberDescriptorMap(
            inheritedDescriptors, memberDescriptors);

    final mergedConstraintDescriptors = _createMergedConstraintDescriptors(
            inheritedDescriptors, constraintDescriptors);

    final mergedMethodDescriptors = _createMergedMethodDescriptorMap(
            inheritedDescriptors, methodDescriptors);

    return new TypeDescriptor._internal(type, mergedConstraintDescriptors,
        mergedMemberDescriptors, mergedMethodDescriptors);
  }

  static Set<ConstraintDescriptor> _createMergedConstraintDescriptors(
      Iterable<TypeDescriptor> inheritedDescriptors,
      Set<ConstraintDescriptor> constraintDescriptors) {

    final mergedDescs =  new Set<ConstraintDescriptor>.identity();
    final inheritedConstraints = inheritedDescriptors
        .map((td) => td.constraintDescriptors)
        .expand((e) => e);

    mergedDescs
        ..addAll(inheritedConstraints)
        ..addAll(constraintDescriptors);
    return mergedDescs;
  }

  static Map<Symbol, MemberDescriptor> _createMergedMemberDescriptorMap(
      Iterable<TypeDescriptor> inheritedDescriptors,
      Set<MemberDescriptor> memberDescriptors) {

    final mm = new Multimap<Symbol, MemberDescriptor>();

    inheritedDescriptors.forEach((td) =>
        td.memberDescriptorsMap.forEach((k, v) {
      mm.add(k, v);
    }));

    memberDescriptors.forEach((m) {
      mm.add(m.name, m);
    });

    final combinedMap = <Symbol, MemberDescriptor>{};

    mm.toMap().forEach((sym, itofmem) {
      combinedMap[sym] = itofmem.skip(1)
          .fold(itofmem.first, (r, v) => r.merge(v));
    });
    final combinedMemberDescriptors = new UnmodifiableMapView(combinedMap);
    return combinedMemberDescriptors;
  }

  static Map<Symbol, FunctionDescriptor> _createMergedMethodDescriptorMap(
       Iterable<TypeDescriptor> inheritedDescriptors,
       Set<FunctionDescriptor> methodDescriptors) {

    final mm = new Multimap<Symbol, FunctionDescriptor>();

    inheritedDescriptors.forEach((td) =>
        td.methodDescriptorsMap.forEach((k, v) {
      mm.add(k, v);
    }));

    methodDescriptors.forEach((m) {
      mm.add(m.name, m);
    });

    final combinedMap = <Symbol, FunctionDescriptor>{};

    mm.toMap().forEach((sym, itofmet) {
      combinedMap[sym] = itofmet.skip(1)
          .fold(itofmet.first, (r, v) => r.merge(v));
    });
    final combinedMethodDescriptors = new UnmodifiableMapView(combinedMap);
    return combinedMethodDescriptors;
  }



  String toString() => 'TypeDescriptor[$type]';

}

class FunctionDescriptor /*extends ElementDescriptor*/ {
//  final Type type; // ??? what do we have here

  final Symbol name;

  /// includes all member constraints inherited from parent classes,
  /// interfaces and mixins
  final Map<Symbol, MemberDescriptor> namedParameterDescriptorsMap;

  // TODO: MemberDescriptor has a mandatory name Symbol but positional
  // params don't have a name
  final List<Option<MemberDescriptor>> positionalParameterDescriptors;

  Iterable<MemberDescriptor> get namedParameterDescriptors =>
      namedParameterDescriptorsMap.values;

  final Option<MemberDescriptor> returnDescriptor;

  FunctionDescriptor(this.name,
      this.positionalParameterDescriptors,
      this.namedParameterDescriptorsMap,
      this.returnDescriptor);


  Option<MemberDescriptor> namedMemberDescriptor(Symbol name) =>
      new Option(namedParameterDescriptorsMap[name]);

  FunctionDescriptor merge(FunctionDescriptor other) {
    ensure(positionalParameterDescriptors.length,
        equals(other.positionalParameterDescriptors.length));

    final mergedPositionalParameters =
        new List.generate(positionalParameterDescriptors.length, (i) => i)
      .map((i) => _mergeMember(positionalParameterDescriptors[i],
          other.positionalParameterDescriptors[i])).toList(growable: false);

    final mergedReturnDescriptor =
        _mergeMember(returnDescriptor, other.returnDescriptor);



    final mm = new Multimap<Symbol, MemberDescriptor>();

    namedParameterDescriptorsMap.forEach((k, v) {
      mm.add(k, v);
    });

    other.namedParameterDescriptorsMap.forEach((k, v) {
      mm.add(k, v);
    });

    final mergedNamedParameters = <Symbol, MemberDescriptor>{};

    mm.toMap().forEach((sym, itofmem) {
      mergedNamedParameters[sym] = itofmem.skip(1)
          .fold(itofmem.first, (r, v) => r.merge(v));
    });
//    final mergedNamedParameters = new UnmodifiableMapView(combinedMap);


    return new FunctionDescriptor(name, mergedPositionalParameters,
        mergedNamedParameters, mergedReturnDescriptor);
  }

  Option<MemberDescriptor> _mergeMember(Option<MemberDescriptor> m1,
      Option<MemberDescriptor> m2) {
    if (m1.isEmpty() && m2.isEmpty()) {
      return const None();
    }

    if (m1.nonEmpty() && m2.isEmpty()) {
      return m1;
    }

    if (m2.nonEmpty() && m1.isEmpty()) {
      return m2;
    }

    return new Some(m1.get().merge(m2.get()));
  }

}

//class MethodDescriptor extends FunctionDescriptor /* with something in common with property??*/ {
// // these need to cope with inherited constraints
//}